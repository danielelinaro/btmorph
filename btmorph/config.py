# default color scheme
c_scheme_default = {'bg': 'white','neurite':['r','g','b','c','m','y','k','g','DarkGray']}

# neuromorpho.org color scheme
c_scheme_nm = {'bg': 'black','neurite':['red','gray','lightgreen','magenta','cyan','cyan','cyan']}

# black on white color scheme
c_scheme_black = {'bg': 'white','neurite':['k','c','k','k','k','k','k','k','r']}

# red on white  color scheme
c_scheme_red = {'bg': 'white','neurite':['r','c','r','r','r','r','r','r','r']}
